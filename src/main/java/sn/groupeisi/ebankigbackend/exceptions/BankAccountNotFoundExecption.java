package sn.groupeisi.ebankigbackend.exceptions;

public class BankAccountNotFoundExecption extends Exception {
    public BankAccountNotFoundExecption(String message) {
        super(message);
    }
}
